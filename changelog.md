# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## v0.0.2 | 2019-10-31

### Added - v0.0.2

- edit feature-login
- edit feature-logout


### Changed

## v0.0.1 | 2017-03-06

### Added - v0.0.1

- New simplified registration form for free users @evanagee
- New stripe responder for successful charges
- Update Salesforce membership record when first payment is received
- Ability for users to edit their membership
- Meta titles/descriptions throughout the app

### Changed - v0.0.1

- Updated registration flow
- Removed title length restriction from resource cards